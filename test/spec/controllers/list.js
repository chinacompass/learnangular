'use strict';

describe('Controller: ListCtrl', function () {

  // load the controller's module
  beforeEach(module('myProjectApp'));

  var ListCtrl,scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListCtrl = $controller('ListCtrl', {
      $scope: scope
    });
  }));

  it('LearnersList Collection Count', function () {
    expect(scope.learnersList.length).toBe(0);
  });

  it('Query Non Learner',function(){
    scope.keywordContent='anyyouwant';
    scope.query();
    expect(scope.learnersList.length).toBe(0);
  });

});
